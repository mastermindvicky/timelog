package com.warner.timelog.service;

import com.warner.timelog.domain.TimeLog;
import com.warner.timelog.domain.TimeLogRepository;
import com.warner.timelog.dto.TimeLogDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by sky on 21/3/2018.
 */
@Service
@Transactional
public class TimeLogService {
    private final TimeLogRepository timeLogRepository;

    public TimeLogService(TimeLogRepository timeLogRepository) {
        this.timeLogRepository = timeLogRepository;
    }

    @Transactional(readOnly = true)
    public TimeLog retrieveTimeLogByEmployeeId(Long id) {

        return timeLogRepository.findOneByEmployeeId(id);
    }

    public TimeLog createTimeLog(TimeLogDTO timeLogDTO)  {
        TimeLog timeLog = new TimeLog();
        timeLog.setProjectId(timeLogDTO.getProjectId());
        timeLog.setEmployeeId(timeLogDTO.getEmployeeId());
        timeLog.setDateOfEntry(java.sql.Date.valueOf(timeLogDTO.getTimeLog()));
        timeLog.setTimeLog(java.sql.Time.valueOf(timeLogDTO.getTimeLog()));
        timeLog.setIsDeleted(false);
        timeLog.setCreatedDate(ZonedDateTime.now());

        timeLogRepository.save(timeLog);

        return timeLog;
    }

    public void deleteTimeLog(List id) {
        List<TimeLog> timeLog = timeLogRepository.findAllById(id);

        for(TimeLog tl: timeLog) {
            tl.setIsDeleted(true);
            timeLogRepository.save(tl);
        }
    }

}
