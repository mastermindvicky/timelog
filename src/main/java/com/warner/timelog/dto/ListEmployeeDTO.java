package com.warner.timelog.dto;

import java.util.List;

/**
 * Created by sky on 22/3/2018.
 */
public class ListEmployeeDTO {
    private List<EmployeeDTO> employeeDTOList;

    public List<EmployeeDTO> getEmployeeDTOList() {
        return employeeDTOList;
    }

    public void setEmployeeDTOList(List<EmployeeDTO> employeeDTOList) {
        this.employeeDTOList = employeeDTOList;
    }
}
