package com.warner.timelog.service;

import com.warner.timelog.domain.Employee;
import com.warner.timelog.domain.EmployeeRepository;
import com.warner.timelog.dto.EmployeeDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by sky on 21/3/2018.
 */
@Service
@Transactional
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Transactional(readOnly = true)
    public Employee retrieveEmployeeById(Long id) {
        return employeeRepository.findOneById(id);
    }

    public Employee createEmployee(EmployeeDTO employeeDTO)  {
        Employee employee = new Employee();
        employee.setEmployeeName(employeeDTO.getEmployeeName());
        employee.setIsDeleted(false);
        employee.setCreatedDate(ZonedDateTime.now());

        employeeRepository.save(employee);

        return employee;
    }

    public void deleteEmployee(List id) {
        List<Employee> employee = employeeRepository.findAllById(id);

        for(Employee emp: employee) {
            emp.setIsDeleted(true);
            employeeRepository.save(emp);
        }
    }


}
