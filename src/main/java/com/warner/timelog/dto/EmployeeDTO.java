package com.warner.timelog.dto;

import com.warner.timelog.domain.Employee;

/**
 * Created by sky on 21/3/2018.
 */
public class EmployeeDTO {
    private Long id;

    private String employeeName;

    public Long getId() {
        return id;
    }

    public EmployeeDTO(){
    }

    public EmployeeDTO(Employee employee) {
        this(employee.getId(), employee.getEmployeeName());
    }

    public EmployeeDTO(Long id, String employeeName) {
        this.id = id;
        this.employeeName = employeeName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }
}
