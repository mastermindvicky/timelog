package com.warner.timelog.service;

import com.warner.timelog.domain.Project;
import com.warner.timelog.domain.ProjectRepository;
import com.warner.timelog.dto.ProjectDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by sky on 21/3/2018.
 */
@Service
@Transactional
public class ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Transactional(readOnly = true)
    public Project retrieveProjectById(Long id) {
        return projectRepository.findOneById(id);
    }

    public Project createProject(ProjectDTO projectDTO) {
        Project project = new Project();
        project.setProjectName(projectDTO.getProjectName());
        project.setIsDeleted(false);
        project.setCreatedDate(ZonedDateTime.now());

        projectRepository.save(project);

        return project;
    }

    public void deleteProject(List id) {
        List<Project> project = projectRepository.findAllById(id);

        for(Project prj: project) {
            prj.setIsDeleted(true);
            projectRepository.save(prj);
        }
    }
}

