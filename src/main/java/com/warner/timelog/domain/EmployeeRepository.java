package com.warner.timelog.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sky on 21/3/2018.
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Employee findOneById(Long id);
}
