package com.warner.timelog.dto;

import com.warner.timelog.domain.Project;

/**
 * Created by sky on 21/3/2018.
 */
public class ProjectDTO {
    private Long id;

    private String projectName;

    public ProjectDTO(){}

    public ProjectDTO(Project project) {
        this(project.getId(), project.getProjectName());
    }

    public ProjectDTO(Long id, String projectName) {
        this.id = id;
        this.projectName = projectName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
