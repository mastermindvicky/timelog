package com.warner.timelog.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sky on 21/3/2018.
 */
public interface ProjectRepository extends JpaRepository<Project, Long> {
    Project findOneById(Long id);
}
