package com.warner.timelog.dto;

import com.warner.timelog.domain.TimeLog;

/**
 * Created by sky on 21/3/2018.
 */
public class TimeLogDTO {

    private Long id;

    private Long employeeId;

    private Long projectId;

    private String timeLog;

    private String dateOfEntry;

    public TimeLogDTO(){}

    public TimeLogDTO(TimeLog timeLog) {
        this(timeLog.getId(), timeLog.getEmployeeId(), timeLog.getProjectId());
    }

    public TimeLogDTO(Long id, Long employeeId, Long projectId) {
        this.id = id;
        this.employeeId = employeeId;
        this.projectId = projectId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getTimeLog() {
        return timeLog;
    }

    public void setTimeLog(String timeLog) {
        this.timeLog = timeLog;
    }

    public String getDateOfEntry() {
        return dateOfEntry;
    }

    public void setDateOfEntry(String dateOfEntry) {
        this.dateOfEntry = dateOfEntry;
    }
}
