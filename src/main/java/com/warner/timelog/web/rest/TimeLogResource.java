package com.warner.timelog.web.rest;

import com.warner.timelog.domain.TimeLog;
import com.warner.timelog.domain.TimeLogRepository;
import com.warner.timelog.dto.TimeLogDTO;
import com.warner.timelog.service.TimeLogService;
import com.warner.timelog.web.rest.util.HeaderUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by sky on 21/3/2018.
 */
@RestController
@RequestMapping("/api")
public class TimeLogResource {
    private static final String ENTITY_NAME = "timelog";

    private final TimeLogService timeLogService;

    private final TimeLogRepository timeLogRepository;


    public TimeLogResource(TimeLogService timeLogService,TimeLogRepository timeLogRepository ) {
        this.timeLogService = timeLogService;
        this.timeLogRepository = timeLogRepository;
    }

    /**
     * POST  /timeLog  : Creates a timeLog.
     * <p>
     * </p>
     *
     * @param timeLogDTO the timeLog to create
     * @return the ResponseEntity with status 201 (Created) and with body the new timeLog, or with status 400 (Bad Request) if the timeLog existed
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/timeLog")
    public ResponseEntity createTimeLog(@RequestBody TimeLogDTO timeLogDTO) throws URISyntaxException {

        if (timeLogDTO.getId() != null) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new timeLog ID is exist."))
                    .body(null);
        } else {
            TimeLog newTimeLog = timeLogService.createTimeLog(timeLogDTO);
            return ResponseEntity.created(new URI("/api/timeLog/" + newTimeLog.getId()))
                    .headers(HeaderUtil.createAlert("A timeLog is created with identifier " + newTimeLog.getId(), newTimeLog.getId().toString()))
                    .body(newTimeLog);
        }
    }

    /**
     * GET  /timeLog/:employeeId : get the timeLog.
     *
     * @param employeeId the id of the timeLog to find
     * @return the ResponseEntity with status 200 (OK) and with body the timeLog, or with status 404 (Not Found)
     */
    @GetMapping("/timeLog/{employeeId}")
    public ResponseEntity<TimeLogDTO> getTimeLogByEmployeeId(@PathVariable Long employeeId) {
        TimeLog timeLog = timeLogService.retrieveTimeLogByEmployeeId(employeeId);
        if (timeLog != null) {
            TimeLogDTO timeLogDTO = new TimeLogDTO(timeLog);
            timeLogDTO.setDateOfEntry(timeLog.getDateOfEntry().toString());
            timeLogDTO.setTimeLog(timeLog.getTimeLog().toString());
            return new ResponseEntity<TimeLogDTO>(timeLogDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * POST  /timeLog  : Remove list of timeLogs.
     * <p>
     * </p>
     *
     * @param timeLogId the timeLog to remove timeLog
     * @return the ResponseEntity with status 201 for update success, or with status 404 (Bad Request) if the timeLog not found
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/timeLog/removeTimeLogs")
    public ResponseEntity removeTimeLog(@PathVariable List<Long> timeLogId) throws URISyntaxException {
        timeLogService.deleteTimeLog(timeLogId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
