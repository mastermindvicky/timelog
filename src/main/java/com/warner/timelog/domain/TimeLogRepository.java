package com.warner.timelog.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sky on 21/3/2018.
 */
public interface TimeLogRepository extends JpaRepository<TimeLog, Long> {
    TimeLog findOneByEmployeeId(Long id);
}
