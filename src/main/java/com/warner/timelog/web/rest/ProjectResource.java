package com.warner.timelog.web.rest;

import com.warner.timelog.domain.Project;
import com.warner.timelog.domain.ProjectRepository;
import com.warner.timelog.dto.ProjectDTO;
import com.warner.timelog.service.ProjectService;
import com.warner.timelog.web.rest.util.HeaderUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by sky on 21/3/2018.
 */
@RestController
@RequestMapping("/api")
public class ProjectResource {
    private static final String ENTITY_NAME = "project";

    private final ProjectService projectService;

    private final ProjectRepository projectRepository;


    public ProjectResource(ProjectService projectService, ProjectRepository projectRepository) {
        this.projectService = projectService;
        this.projectRepository = projectRepository;
    }

    /**
     * POST  /project  : Creates a new project.
     * <p>
     * </p>
     *
     * @param projectDTO the project to create
     * @return the ResponseEntity with status 201 (Created) and with body the new project, or with status 400 (Bad Request) if the project existed
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/project")
    public ResponseEntity createProject(@RequestBody ProjectDTO projectDTO) throws URISyntaxException {

        if (projectDTO.getId() != null) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new project ID is exist."))
                    .body(null);
        } else {
            Project newProject = projectService.createProject(projectDTO);
            return ResponseEntity.created(new URI("/api/project/" + newProject.getId()))
                    .headers(HeaderUtil.createAlert("A project is created with identifier " + newProject.getId(), newProject.getId().toString()))
                    .body(newProject);
        }
    }

    /**
     * GET  /project/:projectId : get the project.
     *
     * @param projectId the id of the project to find
     * @return the ResponseEntity with status 200 (OK) and with body the project, or with status 404 (Not Found)
     */
    @GetMapping("/project/{projectId}")
    public ResponseEntity<ProjectDTO> getProjectById(@PathVariable Long projectId) {
        Project project = projectService.retrieveProjectById(projectId);
        if (project != null) {
            ProjectDTO projectDTO = new ProjectDTO(project);
            return new ResponseEntity<ProjectDTO>(projectDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * POST  /project  : Remove list of projects.
     * <p>
     * </p>
     *
     * @param projectId the project to remove project
     * @return the ResponseEntity with status 201 for update success, or with status 404 (Bad Request) if the project not found
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/project/removeProjects")
    public ResponseEntity removeProject(@PathVariable List<Long> projectId) throws URISyntaxException {
        projectService.deleteProject(projectId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
