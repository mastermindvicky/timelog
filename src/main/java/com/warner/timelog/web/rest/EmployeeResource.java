package com.warner.timelog.web.rest;

import com.warner.timelog.domain.Employee;
import com.warner.timelog.domain.EmployeeRepository;
import com.warner.timelog.dto.EmployeeDTO;
import com.warner.timelog.dto.ListEmployeeDTO;
import com.warner.timelog.service.EmployeeService;
import com.warner.timelog.web.rest.util.HeaderUtil;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;

/**
 * Created by sky on 21/3/2018.
 */
@RestController
@RequestMapping("/api")
public class EmployeeResource {
    private final Logger log = LoggerFactory.getLogger(EmployeeResource.class);

    private static final String ENTITY_NAME = "employee";

    private final EmployeeService employeeService;

    private final EmployeeRepository employeeRepository;


    public EmployeeResource(EmployeeService employeeService, EmployeeRepository employeeRepository) {
        this.employeeService = employeeService;
        this.employeeRepository = employeeRepository;
    }

    /**
     * POST  /employee  : Creates a new employee.
     * <p>
     * </p>
     *
     * @param listEmployeeDTO the employee to create
     * @return the ResponseEntity with status 201 (Created) and with body the new employee, or with status 400 (Bad Request) if the employee existed
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/employee")
    public ResponseEntity createEmployee(@RequestBody ListEmployeeDTO listEmployeeDTO) throws URISyntaxException {
        log.debug("REST request to create list of Employees : {}", listEmployeeDTO);
        List<Employee> employeeList = new ArrayList<>();
        for(EmployeeDTO empDTO: listEmployeeDTO.getEmployeeDTOList()) {
            if (empDTO.getId() != null) {
                return ResponseEntity.badRequest()
                        .headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new employee ID is exist."))
                        .body(null);
            } else {
                Employee newEmployee = employeeService.createEmployee(empDTO);
                employeeList.add(newEmployee);
                /*return ResponseEntity.created(new URI("/api/employee"))
                        .headers(HeaderUtil.createAlert("A employee is created with identifier " + newEmployee.getId(), newEmployee.getId().toString()))
                        .body(newEmployee);*/
            }
            return ResponseEntity.created(new URI("/api/employee"))
                    .headers(HeaderUtil.createAlert("All Employee is created with identifier ", employeeList.toString()))
                    .body(employeeList);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * GET  /employee/:employeeId : get the employee.
     *
     * @param employeeId the id of the employee to find
     * @return the ResponseEntity with status 200 (OK) and with body the employee, or with status 404 (Not Found)
     */
    @GetMapping("/employee/{employeeId}")
    public ResponseEntity<EmployeeDTO> getEmployeeById(@PathVariable Long employeeId) {
        log.debug("REST request to retrieve Employee with Id : {}", employeeId);
        Employee employee = employeeService.retrieveEmployeeById(employeeId);
        if(employee != null) {
            EmployeeDTO employeeDTO = new EmployeeDTO(employee);
            return new ResponseEntity<EmployeeDTO>(employeeDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * POST  /employee  : Remove list of employees.
     * <p>
     * </p>
     *
     * @param employeeId the employee to remove employee
     * @return the ResponseEntity with status 201 for update success, or with status 404 (Bad Request) if the employee not found
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/employee/removeEmployees")
    public ResponseEntity removeEmployee(@RequestParam("employeeId") List<Long> employeeId) throws URISyntaxException {
        log.debug("REST request to delete list of Employee with Ids : {}", employeeId);
        employeeService.deleteEmployee(employeeId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
